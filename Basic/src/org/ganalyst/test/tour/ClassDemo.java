package org.ganalyst.test.tour;

public class ClassDemo {

	public int pubVariable=0;
	private int prvVariable=0;
	static private int statVariable=0;
	
	public void showPrivate(){
		showMessage(prvVariable);
	}
	//static method
	static public void setStatVar(int x){
		statVariable = x;
	}
	
	public void byRefCall(ClassDemo x, int y){
		x.pubVariable = y;
	}
	
	public void showStatVar(){
		showMessage(statVariable);
	}
	//it is overloading
	public void showMessage(int x){
		System.out.println("Number:" + x);
	}
	public void showMessage(String x){
		System.out.println(x);
	}

}
