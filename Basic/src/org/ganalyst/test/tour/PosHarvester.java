package org.ganalyst.test.tour;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JFileChooser;

public class PosHarvester {

	public static void main(String[] args) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("data/"));
		if (fc.showOpenDialog(null)!=JFileChooser.APPROVE_OPTION){
			return;
		};
		File iFile = fc.getSelectedFile();
		//iFile = new File("data/testSam_1000.txt");
		ArrayList<String[]> buff = new ArrayList<String[]>();
		if(iFile== null) return;
		try {
			BufferedReader br = new BufferedReader(new FileReader(iFile));
			String line = br.readLine();
			while(line != null){
				//split string by \t
				String[] sline = line.split("\t");
				//fiter
				if(sline[2].equals("chr7")){
					buff.add(sline);
				}
				line = br.readLine();
			}
			
			Comparator pos = new Comparator<String[]>() {
			    @Override
			    public int compare(String[] o1,String[] o2) {
			    	int p1 = Integer.valueOf(o1[3]);
			    	int p2 = Integer.valueOf(o2[3]);
			        return Integer.compare(p1, p2);
			    }
			};
			Collections.sort(buff, pos);
			br.close();
			for(String[] s :buff){
				System.out.println(s[3]);

			}
			
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
