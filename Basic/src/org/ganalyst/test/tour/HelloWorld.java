package org.ganalyst.test.tour;

public class HelloWorld {

	public static void main(String[] args) {
		//Create object from class
		ClassDemo obj1 = new ClassDemo();
		ClassDemo obj2 = new ClassDemo();
		obj1.pubVariable = 10; //access by everyone
		obj1.showMessage("show obj1 pub");
		obj1.showMessage(obj1.pubVariable); 
		obj1.showMessage("show obj2 pub");
		obj2.showMessage(obj2.pubVariable);//variable of obj1, obj2 are independent
		obj1.showMessage("pass obj2 to obj1 byRefCall(obj2,9)");
		obj1.byRefCall(obj2, 9);
		obj1.showMessage("method is done, obj2 pub do change!");
		obj1.showMessage(obj2.pubVariable);//variable of obj1, obj2 are independent
		
		obj1.showMessage("show obj1 stat ");
		obj1.showStatVar();
		//obj1.showMessage(obj1.prvVariable); <= cannot get private variable
		obj1.showMessage("change stat to 10");
		//Call static method from class directly
		//private variable can be access by internal method
		ClassDemo.setStatVar(10);
		obj1.showMessage("show obj1 stat");
		obj1.showStatVar();
		obj1.showMessage("show obj2 stat");
		obj2.showStatVar();
		
	}

}
