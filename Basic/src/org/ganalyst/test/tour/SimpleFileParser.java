package org.ganalyst.test.tour;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;

public class SimpleFileParser {

	public static void main(String[] args) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("data/"));
		if (fc.showOpenDialog(null)!=JFileChooser.APPROVE_OPTION){
			return;
		};
		File iFile = fc.getSelectedFile();
		//iFile = new File("data/testSam_1000.txt");
		StringBuilder sb = new StringBuilder();
		if(iFile== null) return;
		try {
			BufferedReader br = new BufferedReader(new FileReader(iFile));
			String line = br.readLine();
			while(line != null){
				//Add parsing logic here
				
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			br.close();
			System.out.println(sb.toString());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
