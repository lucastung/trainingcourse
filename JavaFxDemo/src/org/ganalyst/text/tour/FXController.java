package org.ganalyst.text.tour;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;

public class FXController {
	@FXML
	Button press = new Button();
	@FXML
	Button load = new Button();
	@FXML
	MenuItem delete; // = new MenuItem();

	@FXML
	TextArea out = new TextArea();
	
	@FXML
	private void onPress(ActionEvent e){
		Button p = (Button)e.getSource();
		if(p.getText().equals("Load")){
			FileChooser fc = new FileChooser();
			fc.setInitialDirectory(new File("data/"));
			File iFile = fc.showOpenDialog(null);
			if(iFile != null){
				try {
					String data = new String (Files.readAllBytes(iFile.toPath()));
					out.setText(data);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			
		}else out.setText(p.getText()+  " say: hello world");
	
	}
	
	
	
	
	
	
	
}
